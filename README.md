# XTORRAGE
File storage with HTTP API

## Installation
1. Clone repository and go to project folder:

    ```
    $ cd /path/to/your/project    
    ```

2. Install dependencies:

    ```
    $ composer install
    ``` 

3. Run Docker:

    ```
    $ docker-compose up
    ```

4. Add to your `/etc/hosts`:
    
    ```
    127.0.0.1 web.xtorrage
    ```

## HTTP API

### Get file list
`GET http://web.xtorrage/api/files`

### Get file content
`GET http://web.xtorrage/api/files/xsolla.pdf`

### Get file metadata
`GET http://web.xtorrage/api/metadata/xsolla.pdf`

### Create or update file
`PUT http://web.xtorrage/api/files/Game.of.Thrones.s07e07.rus.LostFilm.TV.avi`

Content of file should be sended in request body.

To replace existsing file, use `overwrite=true` parameter:

`PUT http://web.xtorrage/api/files/doNotReplace.xlsx?overwrite=true`

## Coming soon

Authentification, read/write access, web interface, optimizations, etc.