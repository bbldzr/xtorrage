<?php

namespace app\controllers;

use Yii;
use app\storage\File;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class ApiController extends Controller
{
    public $enableCsrfValidation = false;

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionFiles()
    {
        return Yii::$app->storage->getList();
    }

    public function actionGet($file)
    {
        $file = $this->getFile($file);
        $this->releaseFile($file);
    }

    /**
     * @param File $file
     */
    private function releaseFile($file)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;

        $headers = Yii::$app->response->headers;
        $headers->set('X-Accel-Redirect', '/internal-download/' . $file->name);

        $contentType = $file->getMimeType();
        $headers->set('Content-Type', $contentType);
    }


    public function actionMetadata($file)
    {
        $file = $this->getFile($file);
        return $file->getMeta();
    }

    /**
     * @param string $file
     * @return File
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    private function getFile($file)
    {
        $this->checkFilename($file);
        $file = Yii::$app->storage->getFile($file);
        if (!$file) {
            throw new NotFoundHttpException('File not found');
        }
        return $file;
    }

    public function actionUpload($file, $overwrite = false)
    {
        $this->checkFilename($file);
        if ($overwrite !== 'true') {
            if (Yii::$app->storage->getFile($file)) {
                throw new ForbiddenHttpException('File exists');
            }
        }
        $result = Yii::$app->storage->uploadFile($file, Yii::$app->request);
        if ($result) {
            return $result;
        } else {
            throw new ForbiddenHttpException('Error uploading file.');
        }
    }

    /**
     * @param $file
     * @throws BadRequestHttpException
     */
    public function checkFilename($file)
    {
        $fileName = pathinfo($file, PATHINFO_BASENAME);
        if ($fileName !== $file) {
            throw new BadRequestHttpException('Incorrect file name');
        }
    }
}
