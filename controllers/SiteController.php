<?php

namespace app\controllers;

use yii\web\Controller;
use Yii;

class SiteController extends Controller
{
    public function actionIndex()
    {
       $this->redirect('/api/files');
    }
}