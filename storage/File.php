<?php

namespace app\storage;

use yii\helpers\FileHelper;
use Yii;
use yii\web\Request;

class File
{
    public
        $name,
        $path,
        $mimeType,
        $size,
        $modified;

    public function __construct($name, $path)
    {
        $this->name = $name;
        $this->path = $path;
    }

    public function getMimeType()
    {
        if ($this->mimeType === null) {
            $this->mimeType = FileHelper::getMimeType($this->getPath());
        }
        return $this->mimeType;
    }

    public function getPath()
    {
        return $this->path . '/' . $this->name;
    }

    public function getMeta()
    {
        return [
            'name' => $this->name,
            'size' => $this->getSizeFormatted(),
            'bytes' => $this->getSize(),
            'mime_type' => $this->getMimeType(),
            'modified' => $this->getModified(),
        ];
    }

    public function getSizeFormatted()
    {
        return Yii::$app->formatter->asShortSize($this->getSize());
    }

    public function getSize()
    {
        if ($this->size === null) {
            $this->size = filesize($this->getPath());
        }
        return $this->size;
    }

    public function getModified()
    {
        if ($this->modified === null) {
            $this->modified = date('Y-m-d H:i:s', filemtime($this->getPath()));
        }
        return $this->modified;
    }

    public function delete()
    {
        unlink($this->getPath());
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function saveFromInput($request)
    {
        $src = fopen('php://input', 'r');
        $dest = fopen($this->getPath(), 'w');
        $size = stream_copy_to_stream($src, $dest);
        fclose($src);
        fclose($dest);
        if ($size === false) {
            return false;
        }
        return true;
    }
}