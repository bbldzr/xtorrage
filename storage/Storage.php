<?php

namespace app\storage;

use yii\base\Component;
use yii\helpers\FileHelper;
use Yii;
use yii\web\Request;
use yii\web\UploadedFile;

class Storage extends Component
{
    public $path;

    private $files;

    public function init()
    {
        parent::init();
        $this->path = Yii::getAlias($this->path);
        if (!file_exists($this->path)) {
            mkdir($this->path, 0777, true);
        }
    }

    public function getList()
    {
        return array_keys($this->getFiles());
    }

    private function getFiles()
    {
        if ($this->files === null) {
            $files = FileHelper::findFiles($this->path, [
                'except' => [ '.*'],
                'recursive' => false,
            ]);
            foreach ($files as &$file) {
                $file = pathinfo($file, PATHINFO_BASENAME);
                $this->addFile($file);
            }
        }
        return $this->files;
    }

    public function getContentType($fileName)
    {
        return $this->getFile($fileName)->getContentType();
    }

    /**
     * @param $fileName
     * @return bool|File
     */
    public function getFile($fileName)
    {
        if (!isset($this->files[$fileName]))
        {
            $path = $this->path . '/' . $fileName;
            if (file_exists($path) && is_file($path))
            {
                $this->addFile($fileName);
            } else {
                return false;
            }
        }
        return $this->files[$fileName];
    }

    private function addFile($name)
    {
        $this->files[$name] = new File($name, $this->path);
    }

    /**
     * @param Request $request
     * @return bool|array;
     */
    public function uploadFile($fileName, $request)
    {
        $file = new File($fileName, $this->path);
        if ($file->saveFromInput($request)) {
            return $file->getMeta();
        }
        return false;
    }

    public function deleteFile($name)
    {
        $file = $this->getFile($name);
        if ($file) {
            $file->delete();
            unset($this->files[$name]);
        }
    }
}